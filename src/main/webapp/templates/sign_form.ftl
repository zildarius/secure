<#ftl encoding="UTF-8"/>

<!DOCTYPE HTML>

<html>
<head>
    <title>${model["title"]}</title>

    <link type="text/css" href="${rc.getContextPath()}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" />

    <script type="application/javascript" src="${rc.getContextPath()}/js/jquery.js"></script>
    <script type="text/javascript" src="${rc.getContextPath()}/js/jquery-ui/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#button").button();
            $( "#form" ).position({
                my: "center",
                of: $(window)
            });
            $("#login, #pass").css({width: '200px'});
        });
    </script>
</head>
<body>

<form action="${rc.getContextPath()}/${model["action"]}" method="post" id="form" style="position: absolute">
    <h2 style="color: #1C94C4">${model["label"]}:</h2>
    <input type="text" name="login" placeholder="Login" required autofocus id="login">
<br>
    <input type="password" name="password" placeholder="Password" required id="pass">
<br>
    <p align="center"><button type="submit" id="button">${model["button_label"]}</button></p>
</form>


</body>
</html>
