<#ftl encoding="UTF-8"/>

<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"] />
<#assign sec=JspTaglibs["http://www.springframework.org/security/tags"] />

<!DOCTYPE HTML>

<html>

<head>

    <title>Регистрация и авторизация</title>

    <link type="text/css" href="${rc.getContextPath()}/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" />

    <script type="application/javascript" src="${rc.getContextPath()}/js/jquery.js"></script>
    <script type="text/javascript" src="${rc.getContextPath()}/js/jquery-ui/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#butEnter, #butReg, #butLogout").button().css({ width: '300px' });
            $( "#content" ).position({
               my: "center",
               of: $(window)
            });
        });
    </script>

</head>
<body>
<div id="content" style="position: absolute">
    <@sec.authorize access="!isAuthenticated()">
        <a href="<@c.url value="/login" />"><input id="butEnter" type="button" value="Войти"></a>
    <br>
        <a href="<@c.url value="/register" />"><input id="butReg" type="button" value="Зарегистрироваться"></a>
    </@sec.authorize>
    <@sec.authorize access="isAuthenticated()">
        <label id="loginLabel">Ваш логин: </label><h3><@sec.authentication property="principal.username" /></h3></p>
    <br>
        <a href="<@c.url value="/logout" />"><input id="butLogout" type="button" value="Выйти"></a>
    </@sec.authorize>
</div>
</body>
</html>
