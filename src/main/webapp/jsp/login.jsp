<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Авторизация</title>
</head>
<body>


<c:url value="/j_spring_security_check" var="loginUrl" />
<form action="${loginUrl}" method="post">
    <h2>Please sign in</h2>
    <input type="text" name="j_username" placeholder="Email address" required autofocus>
    <input type="password" name="j_password" placeholder="Password" required>
    <button type="submit">Войти</button>
</form>


</body>
</html>
