package ru.devgr.secure.exception;

public class SecureCommonException extends Exception {

    public SecureCommonException(String message) {
        super(message);
    }

    public SecureCommonException(String message, Throwable th) {
        super(message, th);
    }

}
