package ru.devgr.secure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.devgr.secure.dao.entity.User;
import ru.devgr.secure.exception.SecureCommonException;
import ru.devgr.secure.service.AuthService;

import java.util.HashMap;

@Controller
@RequestMapping()
public class MainController {

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start() {
        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@ModelAttribute("model") HashMap model){
        model.put("title","Авторизация");
        model.put("action","j_spring_security_check");
        model.put("label","Авторизоваться");
        model.put("button_label","Войти");

        return "sign_form";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(){
        return "logout";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerPage(@ModelAttribute("model") HashMap model){
        model.put("title","Регистрация");
        model.put("action","register");
        model.put("label","Заполните форму");
        model.put("button_label","Зарегистрироваться");

        return "sign_form";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("registerform") User user) throws SecureCommonException {
        authService.addUser(user.getLogin(), user.getPassword());
        return "index";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String errorPage(){
        return "error";
    }

}