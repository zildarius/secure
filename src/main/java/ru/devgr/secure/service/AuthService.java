package ru.devgr.secure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.devgr.secure.dao.entity.User;
import ru.devgr.secure.dao.entity.UserRoleEnum;
import ru.devgr.secure.exception.SecureCommonException;

@Service
@Transactional
public class AuthService {

    @PersistenceContext
    private EntityManager em = Persistence.createEntityManagerFactory("auth").createEntityManager();
    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;
    @Autowired
    private SaltSource saltSource;

    public User getUser(String login){
        Query query = em.createNamedQuery("User.findByLogin", User.class);
        query.setParameter("login", login);
        List results = query.getResultList();
        if (results.isEmpty()){
            return null;
        }
        if (results.size() > 1){
            // ToDo необходимо определить как решать эту ситуацию
        }

        User user = (User)query.getSingleResult();
        return user;
    }

    public void addUser(String login, String password) throws SecureCommonException{
        try {
            if (getUser(login) != null){
                throw new SecureCommonException("Такой пользователь уже существует в системе.\n" +
                        "Пожалуйста, выберете другой логин.");
            }
            Set<GrantedAuthority> roles = new HashSet();
            roles.add(new SimpleGrantedAuthority(UserRoleEnum.USER.name()));
            UserDetails userDetails =
                    new org.springframework.security.core.userdetails.User(login, password, roles);
            final String encryptedPassword = shaPasswordEncoder.encodePassword(password, saltSource.getSalt(userDetails));
            User user = new User(login, encryptedPassword);
            user.setTmp(password);
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
        }catch (Exception exc){
            throw new SecureCommonException("При добавлении пользователя произошла ошибка.\n" +
                    "Пожалуйста, попробуйте позднее.\n" + exc.getMessage());
        }
    }


}
